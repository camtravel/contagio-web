<?php
function date_in_microtime()
{
    return number_format(microtime(true)*1000,0,'.','');
}

function to_default_date($time){
    if($time == 0){
        $datetime = '0000-00-00 00:00:00';
    }else{
        $time = $time/1000;
        $datetime = date('Y-m-d H:i:s',$time);
    }
    return $datetime;
}

