<?php

if (!function_exists('millistime')) {
        
        function millistime(){
                return round(microtime(true) * 1000);
        }
}