<?php

function phpmailer_send($param = array(), $ishtml = false) {
	/* $param = array(
	 *   'from'     => '',
	 *   'fromname' => '',
	 *   'to'       => '',
	 *   'subject'  => '',
	 *   'message'  => ''
	 * );
	 */

	$param['from'] = isset($param['from']) ? $param['from'] : MAIL_FROM_SERVER;
	$param['fromname'] = isset($param['fromname']) ? $param['fromname'] : MAIL_FROMNAME_SERVER;
	$param['debug'] = isset($param['debug']) ? $param['debug'] : FALSE;

	require_once APPPATH . 'libraries/phpmailer/class.phpmailer.php';

	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->Host = MAIL_HOST_SERVER;
	$mail->Port = MAIL_PORT_SERVER;
	$mail->SMTPAuth = MAIL_AUTH_SERVER;
	$mail->SMTPSecure = MAIL_SECURE_SERVER;
	$mail->SMTPDebug = $param['debug'];
	$mail->Username = MAIL_USERNAME_SERVER;
	$mail->Password = MAIL_PASSWORD_SERVER;
	$mail->From = $param['from'];
	$mail->FromName = $param['fromname'];
	$mail->CharSet = 'UTF-8';
	if (isset($param['ishtml']))
		$mail->IsHTML($param['ishtml']);

	if (isset($param['attachment']))
		$mail->AddAttachment($param['attachment']);

	if (is_array($param['to'])) {
		foreach ($param['to'] as $a_to) {
			$mail->AddAddress($a_to, '');
		}
	} else {
		$mail->AddAddress($param['to'], '');
	}
	if (isset($param['cc'])) {
		if (is_array($param['cc'])) {
			foreach ($param['cc'] as $a_cc) {
				$mail->AddCC($a_cc, '');
			}
		} else {
			$mail->AddCC($param['cc'], '');
		}
	}

	//$mail->AddReplyTo($mail->From, $mail->FromName);
	$mail->WordWrap = MAIL_WORDWRAP_SERVER;
	$mail->IsHTML($ishtml);

	$mail->Subject = $param['subject'];
	$mail->Body = $param['message'];

	// return $mail->Send();
	if(!$mail->Send()){
		return $mail->ErrorInfo;
	}else{
		return true;
	}
}

function mail_send($param = array(), $ishtml = false){
	$to = "";
        if (!isset($param['to'])) {
            return false;// add false
        }
        if (is_array($param['to'])) {
            $to = implode(",", $param['to']);
        } else {
            $to = $param['to'];
        }

        $subject = $param['subject'];
        $message = $param['message'];

        $headers = "";
        if($ishtml){
            // To send HTML mail, the Content-type header must be set
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        }
       
        $headers .= "From: " . MAIL_FROMNAME . "<" . MAIL_FROM . ">". "\r\n";
        mail($to,$subject,$message,$headers,"-f".MAIL_USERNAME);
		
		return mail($to,$subject,$message,$headers,"-f".MAIL_USERNAME);
}