<?php
require_once(APPPATH . '/libraries/OpaqueEncoder.php');

class Auth_token extends CI_Model {
	function generate_auth_token($user_id) {
		$base_token = bin2hex(openssl_random_pseudo_bytes(16));
		
		// use this if openssl is not available
		// $base_token = md5(uniqid(mt_rand() . $user_id, true));
		
		$this->db->insert('auth_token', array(
			'secret'  => $base_token,
			'user_id' => $user_id,
			'device_id' => '',
			'expiry' => strtotime("+2 week")
			));

		$id = $this->db->insert_id();
		
		$encoder = new OpaqueEncoder(OPAQUE);
		$id_hex = $encoder->encode($id);
		
		$auth_token = substr($id_hex, 0, 4) . $base_token . substr($id_hex, 4, 4) ;

		return $auth_token;
	}

	function check_auth_token($auth_token) {
		$retval = FALSE;

		$base_token = substr($auth_token, 4, 32);
		$id_hex = substr($auth_token, 0, 4) . substr($auth_token, 36, 4);

		$encoder = new OpaqueEncoder(OPAQUE);
		$id = $encoder->decode($id_hex);
		
		$this->db->select('user_id');
		$this->db->from('auth_token');
		$this->db->where('id', $id);
		$this->db->where('secret', $base_token);
		$this->db->where('expiry > ', strtotime("now"));
		
		$query = $this->db->get();

		if($row = $query->row_array()) {
			$retval = $row['user_id'];
		}

		return $retval;
	}
	
	function delete_auth_token($user_id, $auth_token) {
		$base_token = substr($auth_token, 32);
		
		$this->db->where('user_id', $user_id);
		$this->db->where('secret', $base_token);
		$this->db->delete('auth_token');
	}
}
