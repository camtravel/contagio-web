<?php
class User extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get("user");
	}

	function add($data){
		$this->load->library('hash');
		if($data['password'] != ""){
			$data['password'] = $this->hash->hash_password($data['password']);
		}
		$this->db->insert('user',$data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->load->library('hash');
		if($data['password'] != ""){
			$data['password'] = $this->hash->hash_password($data['password']);
		}
		$this->db->where('id', $id);
		return $this->db->update('user',$data);
	}

	function auth($email, $password) {
		$retval = array();
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			
			if ($this->check_password($password, $user['password'])) {
				unset($user['password']);			
				$retval = $user;
			}
		}
		
		return $retval;
	}

	function check_password($password, $hash) {
		$this->load->library('hash');
		return $this->hash->check_password($password, $hash);
	}

	function auth_with_facebook($email) {
		$retval = array();
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			
			unset($user['password']);			
			$retval = $user;
		}
		
		return $retval;
	}

	function generate_password($length = 8) {
		$charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$pass = '';
		srand((float) microtime() * 10000000);
		for ($i = 0; $i < $length; $i++) {
			$pass .= $charset[rand(0, strlen($charset) - 1)];
		}
		return $pass;
	}
}
?>