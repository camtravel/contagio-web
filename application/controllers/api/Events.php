<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once (APPPATH . 'libraries/API_Controller.php');

class Events extends API_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('event');	
	}

	function add(){
		$this->check_auth_token();
		$data = json_decode(file_get_contents('php://input'), true);

		$data = array("user_id" => $data['user_id'],
					  "location_id" => $data['location_id'],
					  "name" => $data['name'],
					  "datetime" => $data['datetime'],
					  "organizer" => $data['organizer'],
					  "created_at" => $data['created_at'],
					  "updated_at" => $data['updated_at']
					  );

		if($id = $this->event->add($data)){
			$data["id"] = $id;
			$result = array(	
				'status' => 1,
				'msg' => 'Event has been successfully saved',
				'data' => $data
			);
		}else{
			$result = array(
				'status' => 0,
				'msg' => 'Something error occurred in the server. Your event cannot be saved at this time. Please try again later.'
			);
		}

		$this->response($result);
	}

	function upload_image() {
        $this->check_auth_token();
		
        if ($_FILES["file"]["error"] > 0) {
            $this->response(array(
                'status' => 0,
                'message' => $_FILES["file"]["error"],
            ));
        } else {
            $this->load->model('event_image');
            $event_id = $this->input->post('event_id');
            $name = $this->input->post('name').".jpg";
            
            $is_linked = $this->event->get(array('id' => $event_id))->row_array();

            if (!$is_linked) {
                $this->response(array(
                    'status' => 0,
                    'msg' => "Event data not found"
                ));
            }

           	$file_name = $event_id . '_' . $name;
           	$dest_file = PATH_UPLOAD_TO_EVENT_ATTACHMENT . DIRECTORY_SEPARATOR . $file_name;
            move_uploaded_file($_FILES["file"]["tmp_name"], $dest_file);

            $img = array(
                'event_id' => $event_id,
                'name' => $file_name,
                'created_at' => $this->input->post('created_at'),
                'updated_at' => $this->input->post('updated_at')
            );
            
            $img_id = $this->event_image->add($img);

            $this->response(array(
                'status' => 1,
                'event_id' => $this->input->post('event_id'),
                'id_server' => $img_id,
                'name' => $name,
                'img' => $img,
                'msg' => "Event has been successfully saved"
            ));
        }
    }

    function get_images(){
        $event_id = $this->input->post('event_id');

        $this->load->model('event_image');
        $data = $this->event_image->get(array('event_id' => $event_id))->result_array();

        if ($data) {
            $result = array(
                'status' => 1,
                'data' => $data
            );
        } else {
            $result = array(
                'status' => 0,
                'msg' => 'Data not found'
            );
        }

        $this->response($result);
    }
}